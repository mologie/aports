# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.39.0
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x, mips, mips64: limited by cargo
arch="all !s390x !mips !mips64 !riscv64"
license="MIT"
makedepends="cargo pkgconf openssl1.1-compat-dev libx11-dev libxcb-dev libgit2-dev python3"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz
	0001-remove-dataframe-feature-from-default.patch"

# The parquet crate required by dataframe has some implementations that depend on specific architectures
case "$CARCH" in
	aarch64 | arm* | x86*) _cargo_opts="--locked --features extra,dataframe" ;;
	*) _cargo_opts="--locked --features extra" ;;
esac

build() {
	cargo build --release $_cargo_opts
}

check() {
	# Some tests may timeout on armhf with release build
	cargo test $_cargo_opts
}

package() {
	cargo install $_cargo_opts --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="c3735a22f7b41b78847e5361c300252f4de77a2c783ea219a8f7f56b3211845e19807f216986361b75d6dac0b67340371e1475b3e6515a8c2dac4fc52ac9488a  nushell-0.39.0.tar.gz
f1b87fd1b08641dd2526309806392cbd5267ffc1ee31569874f56217eb26e422994a5a4cce38335c0152775ca186bf75fd7890ec85a6b031cca06318e63dd035  0001-remove-dataframe-feature-from-default.patch"
