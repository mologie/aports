# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=glacier-home
pkgver=0.35.1
pkgrel=0
pkgdesc="The Glacier homescreen"
url="https://github.com/nemomobile-ux/glacier-home"
# armhf blocked by libqofonoext
# s390x and riscv64 blocked by polkit-qt-1
arch="all !armhf !s390x !riscv64"
license="BSD-3-Clause AND MIT"
depends="
	connman
	libqofonoext
	mce
	nemo-qml-plugin-configuration
	nemo-qml-plugin-connectivity
	nemo-qml-plugin-contacts
	nemo-qml-plugin-devicelock
	nemo-qml-plugin-notifications
	nemo-qml-plugin-statusnotifier
	nemo-qml-plugin-time
	qt5-qtfeedback
	qt5-qtgraphicaleffects
	qt5-qtmultimedia
	qtmpris
	qtquickcontrols-nemo
	usb-moded
	"
makedepends="
	bluez-qt-dev
	extra-cmake-modules
	lipstick-dev
	nemo-qml-plugin-devicelock-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	qt5-qtwayland-dev
	"
source="https://github.com/nemomobile-ux/glacier-home/archive/$pkgver/glacier-home-$pkgver.tar.gz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DUSE_SYSTEMD=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e7f045c26661ee91788a72ce7153cce4498603a33fc4410647b9a23828dcfacc336fc463fc82ecb8dbe5bbb7ce19d06790c7c37133d573fa30526a890c6c526d  glacier-home-0.35.1.tar.gz
"
