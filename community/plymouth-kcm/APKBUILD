# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plymouth-kcm
pkgver=5.23.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="https://invent.kde.org/plasma/plymouth-kcm"
pkgdesc="KCM to manage the Plymouth (Boot) theme"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcmutils
	kconfig-dev
	kconfigwidgets-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	plymouth-dev
	qt5-qtbase-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plymouth-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d073941c4fc9815576e9fc93b2d9a13f79ec03e82cf639aa2ddb9ebf3dca645760dd92da81e565d16d7cb830b89a44047bfce1ab197cc237f597bae26daaa794  plymouth-kcm-5.23.2.tar.xz
"
